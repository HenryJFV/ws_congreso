﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Agenda.aspx.cs" Inherits="Ws2.Agenda" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agenda | Congreso 2017</title>
    <link href="Content/bootstrap3.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                <asp:Image ID="Image1" ImageUrl="~/logo_U.png" Style="width: 30%; height: 10%;" runat="server" />
            </div>
            <div class="card" style="margin: 10px;">
                <div style="padding: 10px;">
                    <div style="margin-bottom: 10px;">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link" href="Index.aspx">Asistentes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active">Agenda</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="Ingreso.aspx">Ingresos</a>
                            </li>
                            <li>
                                <asp:Button runat="server" Text="Cerrar sesión" OnClick="CerrarBtn" CssClass="btn btn-danger" Style="color: white;" />
                            </li>
                        </ul>
                    </div>

                    <div style="padding: 10px;">
                        <asp:GridView BorderStyle="None" CellSpacing="0" CssClass="table table-striped table-bordered" ID="grid_viewAgenda" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="fechahorainicio" HeaderText="Fecha/Hora inicio" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" />
                                <asp:BoundField DataField="fechahoracierre" HeaderText="Fecha/Hora cierre" ItemStyle-BorderStyle="None " HeaderStyle-BorderStyle="None" />
                                <asp:BoundField DataField="descripcion" HeaderText="Descripción" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" />


                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="Scripts/bootstrap-datetimepicker.min.js"></script>
    <script src="Scripts/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/locales/bootstrap-datetimepicker.fr.js"></script>
    <script type="text/javascript">
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    </script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js "></script>
    <script>
        $(document).ready(function () {
            $('#grid_viewAgenda').DataTable();
        });
    </script>
</body>
</html>
