﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Ws2.Manager
{
    public class manager
    {
        public DataTable BringData()
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "SELECT * FROM `user_registered` "+
                                  "WHERE `estado` = '1'";
            conexion.CrearComando(sentenciaSQL);
            return conexion.traerdata() ?? (DataTable)null;
        }

        public DataTable BringDataForId(string id)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "SELECT `identificacion`, `nombres`, `apellidos`, `entidad`, `celular`, `tipo_identificacion`,  `email`,`estado` FROM `user_registered` WHERE `identificacion` = @id";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@id", id);
            return conexion.traerdata() ?? (DataTable)null;
        }

        public DataTable TraerAgenda()
        {
            Conexion.nombrecadena = "bd_congre";

            Conexion conexion = new Conexion();
            string sentenciaSQL = "SELECT * FROM `agenda` ORDER BY `fechahorainicio`, `fechahoracierre`";
            conexion.CrearComando(sentenciaSQL);
            return conexion.traerdata() ?? (DataTable)null;
        }

        public long RegisterData(string identificacion, string nombres, string apellidos, string entidad, string celular, string tipo_identificacion, string email)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "INSERT INTO `user_registered` (`identificacion`, `nombres`, `apellidos`, `entidad`, `celular`, `tipo_identificacion`, "+
                                  "`estado`, `created_day`, `uptated_day`, `email`) VALUES (@identificacion, @nombres, @apellidos,@entidad, @celular, @tipo_identificacion, @estado, " +
                                  "@created_day, @uptated_day, @email)";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@identificacion", identificacion);
            conexion.AsignarParametroCadena("@nombres", nombres);
            conexion.AsignarParametroCadena("@apellidos", apellidos);
            conexion.AsignarParametroCadena("@entidad", entidad);
            conexion.AsignarParametroCadena("@celular", celular);
            conexion.AsignarParametroCadena("@tipo_identificacion", tipo_identificacion);
            conexion.AsignarParametroCadena("@estado", "1");
            conexion.AsignarParametroCadena("@created_day", getFechaAñoHoraActual());
            conexion.AsignarParametroCadena("@uptated_day", getFechaAñoHoraActual());
            conexion.AsignarParametroCadena("@email", email);
            return conexion.guardadataid();
        }

        private bool UpdatedRegisterData(string cod,string identificacion, string nombres, string apellidos, string entidad, string celular, string tipo_identificacion)
        {
            Conexion conexion = new Conexion();
            string sentenciaSQL = "UPDATE `user_registered` SET `identificacion` = @identificacion, `nombres` = @nombres, `apellidos` = @apellidos, " +
                                  "`entidad` = @entidad, `celular` = @celular, `tipo_identificacion` = @tipo_identificacion, `uptated_day` = @uptated_day" +
                                  "WHERE `cod` = @cod";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@identificacion", identificacion);
            conexion.AsignarParametroCadena("@nombres", nombres);
            conexion.AsignarParametroCadena("@apellidos", apellidos);
            conexion.AsignarParametroCadena("@entidad", entidad);
            conexion.AsignarParametroCadena("@celular", celular);
            conexion.AsignarParametroCadena("@tipo_identificacion", tipo_identificacion);
            conexion.AsignarParametroCadena("@estado", "1");
            conexion.AsignarParametroCadena("@uptated_day", getFechaAñoHoraActual());
            conexion.AsignarParametroCadena("@cod", cod);
            return conexion.guardadata();
        }

        private bool StateChangeData(string cod, string estado)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "UPDATE `user_registered` SET `estado` = @estado, `uptated_day` = @uptated_day WHERE `cod` = @cod";
            conexion.CrearComando(sentenciaSQL);     
            conexion.AsignarParametroCadena("@cod", cod);
            conexion.AsignarParametroCadena("@estado", estado);
            conexion.AsignarParametroCadena("@uptated_day", getFechaAñoHoraActual());
            return conexion.guardadata();
        }

        public long RegistrarIngreso(string identificacion, string nombres)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "INSERT INTO ingresaron (identificacion, nombres, createdday)VALUES(@identificacion, @nombres, @created_day)";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@identificacion", identificacion);
            conexion.AsignarParametroCadena("@nombres", nombres);
            conexion.AsignarParametroCadena("@created_day", getFechaAñoHoraActual());
            return conexion.guardadataid();
        }

        public DataTable TraerIngresos()
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "SELECT * FROM `ingresaron`";
            conexion.CrearComando(sentenciaSQL);
            return conexion.traerdata() ?? (DataTable)null;
        }

        public DataTable TraerIngresoID(string id)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "SELECT * FROM `ingresaron` WHERE `identificacion` = @id AND CAST(createdday AS DATE) = CAST(@createdday AS DATE)";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@id", id);
            conexion.AsignarParametroCadena("@createdday", getFechaAñoActual());
            return conexion.traerdata() ?? (DataTable)null;
        }

        public string getFechaAñoHoraActual()
        {
            DateTime localDateTime = DateTime.Now;
            DateTime utcDateTime = localDateTime.ToUniversalTime().AddHours(-5);
            string horares = utcDateTime.ToString("yyyy-MM-dd HH:mm:ss:mm");
            return horares;
        }

        public string getFechaAñoActual()
        {
            DateTime localDateTime = DateTime.Now;
            DateTime utcDateTime = localDateTime.ToUniversalTime().AddHours(-5);
            string horares = utcDateTime.ToString("yyyy-MM-dd");
            return horares;
        }

        public DataRow buscarContra(string contraActual)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "SELECT * FROM `con_usuario` WHERE pass = MD5(@newContra) AND usuario = 'superadmin'";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@newContra", contraActual);
            DataRow tabla = conexion.traerfila();
            if(tabla != null){

                if (tabla["pass"].ToString() != "")
                {
                    return tabla;
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public bool guardarNewContra(string nuevaConta)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "UPDATE `con_usuario` SET `pass` = MD5(@new) WHERE usuario = 'superadmin'";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@new", nuevaConta);
            return conexion.guardadata();
        }

        public bool borrarASistente(string identificacion)
        {
            Conexion.nombrecadena = "bd_congre";
            Conexion conexion = new Conexion();
            string sentenciaSQL = "DELETE FROM `user_registered` WHERE `identificacion` = @new";
            conexion.CrearComando(sentenciaSQL);
            conexion.AsignarParametroCadena("@new", identificacion);
            return conexion.guardadata();
        }
    }

}