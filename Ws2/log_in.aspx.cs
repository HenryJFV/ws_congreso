﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ws2
{
    public partial class log_in : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void ingresar(string data)
        {
            if(new Manager.manager().buscarContra(data) != null){
                Session["activo"] = "ON";
                Response.Redirect("index.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string entrada = inputPassword.Value;
            ingresar(entrada);
        }
    }
}