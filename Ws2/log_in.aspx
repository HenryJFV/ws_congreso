﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="log_in.aspx.cs" Inherits="Ws2.log_in" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>LogIn | Congreso 2017</title>
    <link href="Content/bootstrap3.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div style="max-width: 330px; padding: 15px; margin: 0 auto; margin-top:150px;">
                <asp:Image ID="Image1" ImageUrl="~/logo_U.png" style="width:90%;" runat="server" />
               
                <input runat="server" type="password" id="inputPassword" class="form-control" placeholder="Password" style="margin-top:20px;"/>
                <asp:Button ID="Button1" CssClass="btn btn-lg btn-primary btn-block" style="margin-top:20px;" runat="server" Text="Sign in" OnClick="Button1_Click" />
                
            </div>
        </div>
    </form>
</body>
</html>
