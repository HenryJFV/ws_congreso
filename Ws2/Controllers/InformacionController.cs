﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ws2.Manager;

namespace Ws2.Controllers
{
    public class InformacionController : Controller
    {
        // GET: Informacion
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]//gf: guardar informacion, i: identificacion, n: nombres, a: apellidos, e: entidad, c: celular, ti: tipo identificacion
        public JsonResult gf(string i, string n, string a, string e, string c, string ti, string em)
        {
            long cod = new manager().RegisterData(i, n, a, e, c, ti, em);
            if (cod != -1)
            {
                return this.Json((object)new { success = true, message = "Informacion registrada: " + cod.ToString() }, System.Web.Mvc.JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json((object)new { success = false, message = "Access denied" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult getinfo(string i)
        {
            Conexion.nombrecadena = "bd_congre";

            DataTable info = new manager().BringDataForId(i);
            if (info != null && info.Rows.Count > 0)
            {
                var informacion = info.AsEnumerable().Select(row => new
                {
                    descripcion = row["identificacion"],
                    codgradoscursos = row["nombres"],
                    orden = row["apellidos"],
                    entidad = row["entidad"],
                    celular = row["celular"],
                    tipo_identificacion = row["tipo_identificacion"],
                    estado = row["estado"],
                    email = row["email"]
                });

                return this.Json((object)new { success = true, informacion = informacion }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json((object)new { success = false, message = "Access denied" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult traerAgenda()
        {

            DataTable info = new manager().TraerAgenda();
            if (info != null && info.Rows.Count > 0)
            {
                var informacion = info.AsEnumerable().Select(row => new
                {
                    cod = row["cod"],
                    fecha_hora_inicio = row["fechahorainicio"].ToString(),
                    fecha_hora_cierre = row["fechahoracierre"].ToString(),
                    descripcion = row["descripcion"]
                });

                return this.Json((object)new { success = true, informacion = informacion }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json((object)new { success = false, message = "Access denied" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]//gf: guardar informacion, i: identificacion, n: nombres, a: apellidos, e: entidad, c: celular, ti: tipo identificacion
        public JsonResult ingreso(string i, string n)
        { 
                long cod = new manager().RegistrarIngreso(i, n);
                if (cod != -1)
                {
                    return this.Json((object)new { success = true, message = "Informacion registrada: " + cod.ToString() }, System.Web.Mvc.JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json((object)new { success = false, message = "Access denied" }, JsonRequestBehavior.AllowGet);
                }
        }

        [HttpGet]
        public JsonResult getIngreso(string i)
        {
            Conexion.nombrecadena = "bd_congre";

            DataTable info = new manager().TraerIngresoID(i);
            if (info != null && info.Rows.Count > 0)
            {
                var informacion = info.AsEnumerable().Select(row => new
                {
                    descripcion = row["identificacion"],
                    codgradoscursos = row["nombres"],
                    orden = row["createdday"].ToString()
                });

                return this.Json((object)new { success = true, informacion = informacion }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json((object)new { success = false, message = "Access denied" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}