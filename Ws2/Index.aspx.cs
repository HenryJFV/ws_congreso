﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ws2.Manager;

namespace Ws2
{
    public partial class Index : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["activo"] != null && Session["activo"].ToString() == "ON")
            {
                grid_view.PreRender += GridRedenr_PreRender;
                mostrarInformacion();
            }
            else
            {
                Response.Redirect("Log_in.aspx");
            }
  
        }

        public static void GridRedenr_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.GridView grilla = (System.Web.UI.WebControls.GridView)sender;
            if (grilla.Rows.Count > 0)
            {
                grilla.UseAccessibleHeader = true;
                grilla.HeaderRow.TableSection = System.Web.UI.WebControls.TableRowSection.TableHeader;
                grilla.FooterRow.TableSection = System.Web.UI.WebControls.TableRowSection.TableFooter;
            }
        }

        public void mostrarInformacion()
        {
            DataTable data = new manager().BringData();
            if (data != null)
            {
                grid_view.DataSource = data;
                grid_view.DataBind();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            if (FileUpload1.HasFile)
            {
                string ext = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                if (ext == ".xls" || ext == ".xlsx")
                {
                    Stream stream = FileUpload1.FileContent;
                    var wb = new XLWorkbook(stream);
                    try
                    {
                        //new XLWorkbook(@"E:\Demo_plantilla_syllabus.xlsx");
                        //HOJA 1
                        var planilla = wb.Worksheet(1);
                        //Segunda linea

                        int fila = 2;
                        IXLRow row = planilla.Row(fila);
                        while (!row.IsEmpty())
                        {
                            string Identificacion = row.Cell(1).Value.ToString();
                            string Nombres = row.Cell(2).Value.ToString();
                            string Apellidos = row.Cell(3).Value.ToString();
                            string Email = row.Cell(4).Value.ToString();
                            string Entidad = row.Cell(5).Value.ToString();
                            string Celular = row.Cell(6).Value.ToString();
                            string Tipo_identi = row.Cell(7).Value.ToString();

                            new manager().RegisterData(Identificacion, Nombres, Apellidos,Entidad,Celular,Tipo_identi, Email);

                            fila++;
                            row = planilla.Row(fila);
                        }

                        planilla.Dispose();

                        wb.Dispose();

                    }
                    catch (StackOverflowException)
                    {

                    }
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Archivo no compatible');</script>");
                }

                mostrarInformacion();
            }
        }

        public void CerrarBtn(object sender, EventArgs e)
        {
            Session["activo"] = null;
            Response.Redirect("Log_in.aspx");
        }

        public void cambiarContra(object sender, EventArgs e)
        {
            string contrActual = Password1.Value;
            if (contrActual != "")
            {
                string newContra = Password2.Value;
                if (newContra != "")
                {
                    if (new manager().buscarContra(contrActual) != null)
                    {
                        if (new manager().guardarNewContra(newContra))
                        {
                            Response.Redirect("Log_in.aspx");
                        }
                        else
                        {
                            Response.Write("<script>alert('La contraseña no pude ser guardada')</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Contraseña actual invalida')</script>");

                    }
                }
                else
                {
                    Response.Write("<script>alert('El campo contraseña nueva no debe estar vacío')</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('El campo contraseña no debe estar vacío')</script>");
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btndetails = sender as ImageButton;
            GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            //string cod = grid_view.DataKeys[gvrow.RowIndex].Value.ToString(); //Obtener del DataKey de la Row  
            string identificacion = HttpUtility.HtmlDecode(gvrow.Cells[0].Text);
            if (eliminaRegistro(identificacion))
            {
                mostrarInformacion();
            }
            else
            {
                Response.Write("<script>alert('No se pudo eliminar el registro')</script>");
            }
        }

        [WebMethod]
        public static bool eliminaRegistro(string identificacion)
        {
            bool cadena = false;
            cadena = new manager().borrarASistente(identificacion);
            return cadena;
        }
    }
}