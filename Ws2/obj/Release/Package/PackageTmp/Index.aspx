﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Ws2.Index" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Asistentes | Congreso 2017</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                <asp:Image ID="Image1" ImageUrl="~/logo_U.png" Style="width: 30%; height: 10%;" runat="server" />
            </div>
            <div class="card" style="padding: 10px;">
                <div style="margin-bottom: 10px;">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active">Asistentes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Agenda.aspx">Agenda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Ingreso.aspx">Ingresos</a>
                        </li>
                        <li>
                            <asp:Button runat="server" Text="Cerrar sesión" OnClick="CerrarBtn" CssClass="btn btn-danger" Style="color: white;" />
                        </li>

                    </ul>
                </div>
                <div>
                    <table style="margin: 0 auto">
                        <tr>
                            <td><strong style="margin-right: 20px;">Subir archivo excel(Masivo)</strong> </td>
                            <td>
                                <asp:FileUpload ID="FileUpload1" runat="server" Style="margin-right: 20px;" /></td>
                            <td>
                                <asp:Button Style="margin-right: 20px;" Text="Subir informacion" CssClass="btn btn-primary" OnClick="btnAdd_Click" runat="server" /></td>
                            <td>
                                <asp:HyperLink runat="server" class="alert-link" href="Plantilla_.xlsx">Descargar plantilla</asp:HyperLink></td>
                            <td>
                                <a runat="server" class="btn btn-primary" id="btnCambiarContra" style="color: white; margin-left: 20px;">Cambiar contraseña</a>
                            </td>
                        </tr>
                    </table>

                </div>
                <asp:GridView BorderStyle="None"  CellSpacing="0" CssClass="table table-striped table-bordered" ID="grid_view" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="identificacion" HeaderText="Identificacion" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" />

                        <asp:BoundField DataField="nombres" HeaderText="Nombres" ItemStyle-BorderStyle="None " HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small" />
                        <asp:BoundField DataField="apellidos" HeaderText="Apellidos" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small"/>
                        <asp:BoundField DataField="email" HeaderText="Email" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small"/>
                        <asp:BoundField DataField="entidad" HeaderText="Entidad" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small"/>
                        <asp:BoundField DataField="celular" HeaderText="Celular" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small"/>
                        <asp:BoundField DataField="tipo_identificacion" HeaderText="Tipo de identificacion" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField>
                            <HeaderTemplate>Eliminar</HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Eliminar" ImageUrl="~/delete-bin.png" Height="20px" Width="20px" ItemStyle-HorizontalAlign="Center" OnClick="ImageButton1_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <div class="modal fade" id="modal_cam_contra">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Cambiar contraseña</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <%-- <input type="text"  placeholder="Username" aria-describedby="basic-addon1">--%>
                                <input runat="server" id="Password1" type="password" class="form-control" placeholder="Contraseña actual" />
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input runat="server" id="Password2" type="password" class="form-control" placeholder="Contraseña nueva" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input id="Password3" type="password" class="form-control" placeholder="Confirmar contraseña" />
                                </div>
                            </div>
                            <div class="form-group">
                                <a id="mayor8">La contraseña debe contener almenos 8 caracteres</a></br>
                                <a id="mayus">La contraseña debe tener una letra mayuscula</a></br>
                                <a id="numero">La contraseña debe contener al menos un numero</a></br>
                                <a id="iguales">La contraseña deben ser iguales</a>
                            </div>
                        </div>
                        <a></a>
                        <div class="modal-footer">
                            <asp:Button ID="btnGuardar" disabled="disabled" CssClass="btn btn-primary" OnClick="cambiarContra" runat="server" Text="Guardar cambios" />
                            <%--                            <button type="button" onclick="" runat="server" class="">Guardar cambios</button>--%>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js "></script>
    <script>
        var flag = false;
        $(document).ready(function () {
            $('#grid_view').DataTable();
        });
        $("#btnCambiarContra").click(function () {
            $('#modal_cam_contra').modal('show');
        });

        $("#Password2").keyup(function () {
            validar($(this).val());
        });
        function validar(valor) {
            if (valor.length < 8) {
                $('#mayor8').css('color', 'red');
                flag = true;
            } else {
                $('#mayor8').css('color', 'green');
                flag = false;
            }
            //validate capital letter
            if (valor.match(/[A-Z]/)) {
                $('#mayus').css('color', 'green');
                flag = true;
            } else {
                $('#mayus').css('color', 'red');
                flag = false;
            }
            //validate number
            if (valor.match(/\d/)) {
                $('#numero').css('color', 'green');
                flag = true;
            } else {
                $('#numero').css('color', 'red');
                flag = false;
            }
        }
        $("#Password3").keyup(function () {
            if ($(this).val() == $("#Password2").val()) {
                $('#iguales').css('color', 'green');
                flag = true;
            } else {
                $('#iguales').css('color', 'red');
                flag = false;
            }
            validarDisabledButton(flag);
        });
        function validarDisabledButton(valor) {
            if (valor) {
                $('#btnGuardar').removeAttr("disabled")
            } else {
                $('#btnGuardar').attr('disabled');
            }
        }
    </script>
</body>
</html>
