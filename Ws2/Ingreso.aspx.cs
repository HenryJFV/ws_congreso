﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ws2.Manager;

namespace Ws2
{
    public partial class Ingreso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if(Session["activo"] != null && Session["activo"].ToString() == "ON")
            {
                grid_viewIngresos.PreRender += GridRedenr_PreRender;
                mostrarInformacion();
            }
            else
            {
                Response.Redirect("Log_in.aspx");
            }
        }

        public static void GridRedenr_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.GridView grilla = (System.Web.UI.WebControls.GridView)sender;
            if (grilla.Rows.Count > 0)
            {
                grilla.UseAccessibleHeader = true;
                grilla.HeaderRow.TableSection = System.Web.UI.WebControls.TableRowSection.TableHeader;
                grilla.FooterRow.TableSection = System.Web.UI.WebControls.TableRowSection.TableFooter;
            }
        }
        public void mostrarInformacion()
        {
            DataTable data = new manager().TraerIngresos();
            if (data != null)
            {
                grid_viewIngresos.DataSource = data;
                grid_viewIngresos.DataBind();
            }

        }
        public void CerrarBtn(object sender, EventArgs e)
        {
            Session["activo"] = null;
            Response.Redirect("Log_in.aspx");
        }
    }
}