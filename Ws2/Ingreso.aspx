﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ingreso.aspx.cs" Inherits="Ws2.Ingreso" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Ingresos | Congreso 2017</title>
    <link href="Content/bootstrap3.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                <asp:Image ID="Image1" ImageUrl="~/logo_U.png" Style="width: 30%; height: 10%;" runat="server" />
            </div>
            <div class="card" style="margin: 10px;">
                <div style="padding: 10px;">
                    <div style="margin-bottom: 10px;">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link" href="Index.aspx">Asistentes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="Agenda.aspx">Agenda</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active">Ingresos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-success" id="descargarBtn" style="color:white;">Descargar Excel</a>
                            </li>
                         <li>
                            <asp:Button runat="server" Text="Cerrar sesión" OnClick="CerrarBtn" CssClass="btn btn-danger" style="color:white;" />
                        </li>
                        </ul>
                    </div>

                    <div style="padding: 10px;" id="descargar">
                        <asp:GridView BorderStyle="None" CellSpacing="0" CssClass="table table-striped table-bordered" ID="grid_viewIngresos" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="identificacion" HeaderText="Identificacion" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" />
                                <asp:BoundField DataField="nombres" HeaderText="Nombres" ItemStyle-BorderStyle="None " HeaderStyle-BorderStyle="None" />
                                <asp:BoundField DataField="createdday" HeaderText="Fecha/Hora ingreso" ItemStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" />


                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </form>
        <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="Scripts/bootstrap-datetimepicker.min.js"></script>
    <script src="Scripts/bootstrap-datetimepicker.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/locales/bootstrap-datetimepicker.fr.js"></script>
    <script src="Scripts/FileSaver.js"></script>
    <script type="text/javascript">
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    </script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js "></script>

    <script>
        $(document).ready(function () {
            $('#grid_viewIngresos').DataTable({
              
            });
        });
        $("#descargarBtn").click(function () {
            fnExcelReport2("descargar","Planilla de registro de ingresos");
        });
        function fnExcelReport2(id, name) {
            var tab_text = '\uFEFF';
            tab_text = tab_text + '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
            tab_text = tab_text + '<head>';
            tab_text = tab_text + '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            tab_text = tab_text + '<meta name="ProgId" content="Excel.Sheet" />';
            tab_text = tab_text + '<meta name="Generator" content="Microsoft Excel 11" />';
            tab_text = tab_text + '<title>Sample</title>';
            tab_text = tab_text + '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
            tab_text = tab_text + '<x:Name>Hoja 1</x:Name>';
            tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
            tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook>';
            tab_text = tab_text + '</xml><![endif]--></head><body>';
            tab_text = tab_text + '<table>';
            var exportTable = $('#' + id).clone();
            exportTable.find('input').each(function (index, elem) { $(elem).remove(); });
            exportTable.find('.dataTables_filter').each(function (index, elem) { $(elem).remove(); });
            exportTable.find('.dataTables_length').each(function (index, elem) { $(elem).remove(); });
            exportTable.find('.dataTables_info').each(function (index, elem) { $(elem).remove(); });
            exportTable.find('.dataTables_paginate').each(function (index, elem) { $(elem).remove(); });
            tab_text = tab_text + exportTable.html();
            tab_text = tab_text + '</table></body></html>';

            var fileName = name + '.xls';
            var blob = new Blob([tab_text], { type: "application/vnd.ms-excel" })
            window.saveAs(blob, fileName);
        }
    </script>
</body>
</html>
